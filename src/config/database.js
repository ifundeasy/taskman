import dotenv from 'dotenv';

dotenv.config();

export default {
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD == '' ? null : process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  dialect: process.env.DB_DIALECT,
  timestamps: false,
  logging: process.env.NODE_ENV == 'production' ? false : console.log,
  pool: {
    max: parseInt(process.env.DB_POOL_MAX || 5),
    min: parseInt(process.env.DB_POOL_MIN || 0),
    acquire: parseInt(process.env.DB_POOL_ACQUIRE || 30000),
    idle: parseInt(process.env.DB_POOL_IDLE || 10000)
  }
};
