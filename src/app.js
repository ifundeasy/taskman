'use strict';

import express from 'express';
import path from 'path';
import morgan from 'morgan';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import 'express-async-errors';
import { connectDB } from './libs/connection.js';
import indexRouter from './routes/index';
import taskRouter from './routes/task';

const rawBodySaver = function (req, res, buf, encoding) {
  if (buf && buf.length) {
    req.rawBody = buf.toString(encoding || 'utf8');
  }
};

const app = express();
app.use(cors({ origin: '*' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.raw({ verify: rawBodySaver, type: () => true }));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use((req, res, next) => {
  const { headers, query } = req;
  const body = req.rawBody || JSON.stringify(req.body);

  console.log('');
  console.log(req.method, req.url);
  // console.log('HEADER:', JSON.stringify(headers));
  if (Object.keys(query).length) console.log('QUERY:', JSON.stringify(query));
  if (body) console.log('BODY:', body);
  console.log('...');
  next();
});

app.use('/', indexRouter);
app.use('/task', taskRouter);
app.use(function (req, res, next) {
  res.end("Didn't match a route!");
  next();
});
app.use(function (err, req, res, next) {
  res.status(500).send({ error: 0, message: err.message });
});

connectDB();

export default app;
