import express from 'express';

import * as userController from '../controllers/user.js';
import * as authController from '../controllers/auth.js';

const router = express.Router();

router.get(
  '/',
  async (req, res, next) => {
    await authController.auth(req);
    next();
  },
  (req, res, next) => {
    let message = `Hello ${req.user ? req.user.username : 'stranger'}!`;
    return res.status(200).send(message);
  }
);
router.get('/token', authController.authMiddlewere, (req, res) => {
  return res.json({ message: true });
});
router.post('/registration', userController.create);
router.post('/login', authController.create);
router.get('/logout', authController.authMiddlewere, authController.destroy);

export default router;
