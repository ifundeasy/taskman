import { Router } from 'express';
import * as authController from '../controllers/auth.js';
import * as TaskController from '../controllers/task.js';

var router = Router();
router.get('/', authController.authMiddlewere, TaskController.getAll);
router.post('/add/:roomId', authController.authMiddlewere, TaskController.create);
router.put('/infer', TaskController.infer);

export default router;
