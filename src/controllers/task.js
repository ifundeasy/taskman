import regeneratorRuntime from 'regenerator-runtime';
import { Op } from 'sequelize';
import moment from 'moment';
import { exec } from 'child_process';
import { getIO } from '../libs/socket.js';
import analyze from '../libs/extract.js';
import Task from '../models/task.js';

const { env } = process;

export const getAll = async (req, res) => {
  const tasks = await Task().findAll({
    where: {
      user_id: req.user.id,
      [Op.not]: [{ status: -1 }]
    }
  });

  return res.json({ data: tasks });
};

export const create = async (req, res) => {
  const { roomId } = req.params;
  const { rawBody } = req;
  if (!rawBody) {
    console.log(rawBody);
    return res.status(500).send({ message: 'Invalid input' });
  }

  let parsed;
  try {
    parsed = await analyze(rawBody);
  } catch (e) {
    console.log(e);
    return res.status(500).send({ message: e.message });
  }

  // Handle push notification delay
  const now = new Date();
  const interval = 'daily'; // todo: next development: parse recurrent
  const recurrent = true; // todo: next development: if data has interval, recurrent = true
  const forceStop = parseInt(env.FORCE_REMINDS || 5);
  now.setSeconds(now.getSeconds() + forceStop);

  const data = {
    user_id: req.user.id,
    location: null,
    dueAt: null,
    description: parsed.text,
    raw: parsed.raw,
    status: 1
  };
  if (parsed.dates.length) {
    // todo: next development: more than one timestamp found in much place?
    // temporary choos first timestamp with start key
    data.dueAt = moment(parsed.dates[0].start).format();
    if (parsed.places.length) {
      data.location = parsed.places.join('|');
    }
  } else if (parsed.places.length) {
    data.location = parsed.places.join('|');
  }

  // Insert record to db
  const newData = await Task().create(data);

  // Build task scheduler
  const returnId = (parseInt(Math.random().toString()[3]) + 1) * 2;
  const task = {
    roomId,
    id: newData.id,
    ...data,
    reminder: now,
    interval
  };
  const taskStringify = JSON.stringify(task);
  const command = `node ${__dirname}/../libs/reminder.js '${taskStringify}'`;

  exec(command, function (err, stdout, stderr) {
    if (err || stdout) {
      let message = err;

      if (err) console.error(err);
      if (stdout) {
        message = stdout;
        console.error(stdout);
      }
    } else {
      console.log('Reminder job added:', taskStringify);
    }
  });

  res.json(task);
};

export const infer = async (req, res) => {
  const { id, roomId } = req.body;
  await Task().update({ status: 0 }, { where: { id } });

  const payload = JSON.stringify(req.body);
  const message = `DONE: ${payload}`;
  const io = getIO();

  console.log('Reminder job done:', payload);
  io.to(roomId).emit('done-message', req.body);
  return res.json(req.body);
};
