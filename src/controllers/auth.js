import { exec } from 'child_process';
import moment from 'moment';
import regeneratorRuntime from 'regenerator-runtime';
import User from '../models/user';
import Session from '../models/session';
import { getIO } from '../libs/socket.js';
import { sha256 } from '../libs/sha';

const { env } = process;

async function getLogin(username, password) {
  const user = await User().findOne({
    where: {
      username: username,
      password: password,
      status: 1
    }
  });

  return user;
}

async function createSession(user_id) {
  const now = moment();
  const newSession = await Session().create({
    token: sha256(user_id + now.format()),
    user_id: user_id,
    status: 1,
    validUntil: now.add(env.TOKEN_VALIDITY, 'hours').format()
  });

  return newSession;
}

const auth = async req => {
  const { authorization } = req.headers;
  if (!authorization) {
    return { error: true, message: 'Invalid authorization Bearer token', data: null };
  }

  const split = authorization.split('Bearer');
  if (!split[1]) {
    return { error: true, message: 'Invalid authorization Bearer token', data: null };
  }

  const userToken = split[1].trim();
  const session = await Session().findOne({
    where: { token: userToken, status: 1 }
  });

  if (!session) {
    return { error: true, message: 'Token not found!', data: userToken };
  }

  const now = moment();
  const expiredAt = moment(session.validUntil);
  if (!now.isBefore(expiredAt)) {
    return { error: true, message: 'Token already invalid', data: userToken };
  }

  const user = await User().findOne({
    where: { id: session.user_id }
  });

  if (!user) {
    return { error: true, message: 'User not found!', data: userToken };
  }

  req.user = user;

  // extend session
  session.updatedAt = now.format();
  session.validUntil = now.add(env.TOKEN_VALIDITY, 'hours').format();
  await session.save();

  req.session = session;

  return { error: false, message: null, data: userToken };
};

const authMiddlewere = async (req, res, next) => {
  const checking = await auth(req);

  if (!req.session) {
    return res.status(401).send({ message: checking.message });
  }

  next();
};

const create = async (req, res) => {
  const { username } = req.body;
  const password = sha256(username + req.body.password);

  const user = await getLogin(username, password);
  if (!user) {
    return res.status(401).json({ message: 'Username or password is wrong' });
  }

  const session = await createSession(user.id);
  const result = {
    user: {
      id: user.id,
      username: user.username
    },
    token: session.token,
    validUntil: session.validUntil
  };
  res.json(result);
};

const destroy = async (req, res) => {
  await Session().update({ status: -1 }, { where: { id: req.session.id } });
  req.session = undefined;

  res.json({ message: 'Bye' });
};

export { auth, authMiddlewere, create, destroy };
