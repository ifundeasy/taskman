import { exec } from 'child_process';
import moment from 'moment';
import regeneratorRuntime from 'regenerator-runtime';
import User from '../models/user';
import Session from '../models/session';
import { getIO } from '../libs/socket.js';
import { sha256 } from '../libs/sha';

const { env } = process;

export const create = async function (req, res) {
  const user = User();
  const { name, username, password } = req.body;

  const data = {
    name,
    username,
    password: sha256(username + password),
    status: 1
  };

  try {
    if (!password) throw new Error('Invalid password value');
    if (!username) throw new Error('Invalid username value');

    const checkUser = await user.findOne({
      where: {
        username: data.username
      }
    });
    if (checkUser) {
      // note this! we don't care about "user model" status flag,
      // because username should be unique in records
      throw new Error('Username already exist');
    }
    const newUser = await user.create(data);

    return res.json({
      data: {
        id: newUser.id,
        name: newUser.name,
        username: newUser.username
      }
    });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};
