import Sequelize from 'sequelize';
import * as db from '../libs/connection.js';

export default sequelize => {
  if (!sequelize) sequelize = db.sequelize();

  const { DataTypes } = Sequelize;
  const attributes = {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'id'
    },
    task_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'task_id',
      references: {
        key: 'id',
        model: 'Task'
      }
    },
    user_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'user_id',
      references: {
        key: 'id',
        model: 'User'
      }
    },
    location: {
      type: DataTypes.STRING(100),
      allowNull: true,
      defaultValue: '',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'location'
    },
    dueAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'dueAt'
    },
    description: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: '',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'description'
    },
    raw: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'raw'
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'status'
    }
  };
  const options = {
    timestamp: true,
    freezeTableName: true,
    tableName: 'task',
    comment: '',
    indexes: [
      {
        name: 'task_id',
        unique: false,
        type: 'BTREE',
        fields: ['task_id']
      },
      {
        name: 'user_id',
        unique: false,
        type: 'BTREE',
        fields: ['user_id']
      }
    ]
  };
  const TaskModel = sequelize.define('Task', attributes, options);
  return TaskModel;
};
