import Sequelize from 'sequelize';
import * as db from '../libs/connection.js';

export default sequelize => {
  if (!sequelize) sequelize = db.sequelize();

  const { DataTypes } = Sequelize;
  const attributes = {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: '',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'name'
    },
    username: {
      type: DataTypes.STRING(100),
      allowNull: true,
      defaultValue: '',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'username'
    },
    password: {
      type: DataTypes.STRING(100),
      allowNull: false,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'password'
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'status'
    }
  };
  const options = {
    timestamp: true,
    freezeTableName: true,
    tableName: 'user',
    comment: '',
    indexes: []
  };
  const UserModel = sequelize.define('User', attributes, options);
  return UserModel;
};
