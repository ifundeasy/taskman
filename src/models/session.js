import Sequelize from 'sequelize';
import * as db from '../libs/connection.js';

export default sequelize => {
  if (!sequelize) sequelize = db.sequelize();

  const { DataTypes } = Sequelize;
  const attributes = {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: 'id'
    },
    user_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'user_id',
      references: {
        key: 'id',
        model: 'User'
      }
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'token'
    },
    validUntil: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'validUntil'
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0',
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: 'status'
    }
  };
  const options = {
    timestamp: true,
    freezeTableName: true,
    tableName: 'session',
    comment: '',
    indexes: [
      {
        name: 'user_id',
        unique: false,
        type: 'BTREE',
        fields: ['user_id']
      }
    ]
  };
  const SessionModel = sequelize.define('Session', attributes, options);
  return SessionModel;
};
