import { createHash } from 'crypto';

const sha256 = data => {
  return createHash('sha256').update(data).digest('hex');
};

const sha1 = data => {
  return createHash('sha1').update(data).digest('hex');
};

export { sha256, sha1 };
