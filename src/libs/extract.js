import * as chrono from 'chrono-node';
import TextCleaner from 'text-cleaner';
import language from '@google-cloud/language';

const client = new language.LanguageServiceClient();

const cleanText = text => {
  if (!text) return text;
  let str = text.replace(/DATE_[0-9]/gi, '');
  str = str.replace(/\s[\@]\s/gi, ''); //'~`!@#$%^&*()_+[]\\{}|;\':",./<>?';

  while (true) {
    let newText = str.replace(/^[^a-z0-9A-Z]/gi, '').trim();

    if (newText == str) {
      break;
    }

    str = newText;
  }
  str = TextCleaner(str).condense().removeStopWords().trim();
  str = str.s.substr(0, 1).toUpperCase() + str.s.substr(1);

  return str;
};

const getDates = text => {
  const parsed = chrono.parse(text);
  if (!parsed) return false;
  if (!parsed.length) return false;

  const times = [];
  let pattern = text;
  parsed.forEach(function (data, i) {
    times.push(data);
    pattern = pattern.replace(
      text.substring(data.index, data.index + data.text.length),
      `DATE_${i + 1}`
    );
  });

  return {
    raw: text,
    pattern,
    times
  };
};

const getEntities = async text => {
  // Prepares a document, representing the provided text
  const document = {
    content: text,
    type: 'PLAIN_TEXT'
  };

  // Detects entities in the document
  const [result] = await client.analyzeEntities({ document });
  const entities = result.entities;

  const places = [];
  entities.forEach(entity => {
    if (entity.salience > 0.5 && ['ORGANIZATION', 'LOCATION'].indexOf(entity.type) > -1) {
      places.push({
        text: entity.name,
        type: entity.type,
        score: entity.salience
      });
    }
  });

  return places;
};

const chronoToDate = obj => {
  const now = new Date();

  now.setFullYear(obj.year);
  now.setMonth(obj.month - 1);
  now.setDate(obj.day);
  now.setHours(obj.hour);
  now.setMinutes(obj.minute);
  now.setSeconds(obj.second);
  now.setMilliseconds(0);

  return now;
};

const analyze = async text => {
  let newText = null;
  let dates = [];
  const places = [];
  const timestamps = getDates(text);

  if (timestamps) {
    dates = timestamps.times.map(function (time) {
      const { start, end } = time;
      let endTime;
      let startTime = Object.assign({}, start.impliedValues, start.knownValues);

      startTime = chronoToDate(startTime);
      if (end) {
        endTime = Object.assign({}, end.impliedValues, end.knownValues);
        endTime = chronoToDate(endTime);
      }
      return { start: startTime, end: endTime };
    });
    newText = cleanText(timestamps.pattern);
  } else {
    newText = cleanText(text);
  }

  if (newText) {
    const placeEntities = await getEntities(newText);
    placeEntities.forEach(function (place) {
      places.push(place.text);
      newText = newText.replace(place.text, '');
      newText = cleanText(newText);
    });
  }

  return { raw: text, dates, places, text: newText };
};

export default analyze;
