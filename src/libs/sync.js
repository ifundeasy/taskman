'use strict';

import { connectDB } from './connection';
import User from '../models/user';
import Session from '../models/session';
import Task from '../models/task';

const force = true;
(async function () {
  await connectDB();
  await Task().drop();
  await Session().drop();
  await User().sync({ force });
  await Session().sync({ force });
  await Task().sync({ force });
  process.exit();
})();
