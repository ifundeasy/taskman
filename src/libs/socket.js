import socket from 'socket.io';

let io;

const getIO = () => io;

const setIO = _io => {
  io = _io;
};

const emitEvent = message => {
  getIO().on('connection', (sock, err) => {
    const roomId = sock.handshake.query['roomId'];

    console.log(`Socket client ${roomId}::${sock.id} connected to "${message}" event.`);

    sock.join(roomId);
    sock.on(message, function (data) {
      console.log(`Socket on ${message}:`, data);
      io.to(roomId).emit(message, data);
    });

    sock.on('disconnect', () => {
      sock.leave(roomId);
      console.log(`Socket client ${roomId}::${sock.id} disconnected from "${message}" event.`);
    });
  });
};

export { setIO, getIO, emitEvent };
