#!/usr/bin/env node

/**
 * We using require('child_process').exec for run this file.
 * The "exec" method callback have three parameter: err, stdout, stderr
 * this scheduleJob return nothing,
 * so.. if any err or stdout message, it's an error occurrence!
 */

import axios from 'axios';
import dotenv from 'dotenv';
import nodeSchedule from 'node-schedule';
import regeneratorRuntime from 'regenerator-runtime';

dotenv.config();

const callbackUrl = `http://localhost:${process.env.PORT}/task/infer`;
const data = JSON.parse(process.argv[2]);

nodeSchedule.scheduleJob(
  data.reminder,
  async function (data) {
    try {
      await axios.put(callbackUrl, data);
    } catch (e) {
      console.log(e);
    }
  }.bind(null, data)
);
