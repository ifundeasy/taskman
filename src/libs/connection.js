import Sequelize from 'sequelize';
import config from '../config/database.js';
import regeneratorRuntime from 'regenerator-runtime';

let db;

const connectDB = async () => {
  const client = new Sequelize(config);
  try {
    await client.authenticate();
    console.log('Database: Connection has been established successfully.');
  } catch (error) {
    console.error('Database:', error);
  }

  db = client;
};

const getDB = () => {
  return db;
};

const sequelize = () => {
  return db;
};

const beginTransaction = () => {
  return db.transaction();
};

export { connectDB, getDB, sequelize, beginTransaction };
