$(function () {
  var baseURL = 'http://localhost:3000';

  // Handle session
  var session = localStorage.getItem('taskman');
  if (session) {
    try {
      session = JSON.parse(session);
    } catch (e) {
      console.error('Invalid localstorage value!');
    }

    if (session.token) {
      $.ajax({
        url: baseURL + '/token',
        type: 'GET',
        sync: true,
        headers: {
          Authorization: 'Bearer ' + session.token
        },
        statusCode: {
          401: function (res) {
            console.log(res.responseJSON.message);
            localStorage.removeItem('taskman');
          }
        },
        success: function (res, is) {
          if (res.message) window.location = '/';
        }
      });
    }
  }

  // Handle login
  var usernameInput = $('#usernameInput');
  var passwordInput = $('#passwordInput');
  var submitBtn = $('#submitBtn');
  var login = function () {
    var username = usernameInput.val();
    var password = passwordInput.val();

    $.ajax({
      url: baseURL + '/login',
      type: 'POST',
      data: { username, password },
      statusCode: {
        401: function (res) {
          alert(res.responseJSON.message);
        }
      },
      success: function (res, is) {
        var { token, validUntil } = res;
        if (token && validUntil) {
          localStorage.setItem('taskman', JSON.stringify(res));
          window.location = '/';
        }
      }
    });
  };
  $('form').submit(function () {
    login();
    return false;
  });
  submitBtn.click(function () {
    login();
  });
});
