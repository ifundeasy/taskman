$(function () {
  var baseURL = 'http://localhost:3000';

  // Handle session
  var session = localStorage.getItem('taskman');
  if (session) {
    try {
      session = JSON.parse(session);
    } catch (e) {
      console.error('Invalid localstorage value!');
      window.location = '/login';
    }

    if (session.token) {
      $.ajax({
        url: baseURL + '/token',
        type: 'GET',
        sync: true,
        headers: {
          Authorization: 'Bearer ' + session.token
        },
        statusCode: {
          401: function (res) {
            console.log(res.responseJSON.message);
            localStorage.removeItem('taskman');
            alert('Session expired, please login');
            window.location = '/login';
          }
        },
        success: function (res, is) {
          if (!res.message) window.location = '/login';
        }
      });
    }
  } else {
    alert('Plase login!');
    window.location = '/login';
  }
  // Handle logout
  var logoutBtn = $('#logoutBtn');
  logoutBtn.click(function () {
    $.ajax({
      url: baseURL + '/logout',
      type: 'GET',
      headers: {
        Authorization: 'Bearer ' + session.token
      },
      success: function (res, is) {
        if (res.message) {
          alert(res.message);
          window.location = '/login';
        }
      }
    });
  });

  // Handle loading task
  var messageList = $('#messageList');
  $.ajax({
    url: baseURL + '/task',
    type: 'GET',
    headers: {
      Authorization: 'Bearer ' + session.token
    },
    sync: true,
    success: function (res, is) {
      console.log('ajax get', res);
      res.data.forEach(function (data) {
        var raw = {
          time: data.dueAt,
          place: data.location,
          desc: data.description
        };
        var entry = $(`<li id="task-${data.id}">`);
        entry.append('<p>' + data.raw + '</p>');
        entry.append('<pre>' + JSON.stringify(raw, 0, 2) + '</pre>');
        messageList.append(entry);
        if (!data.status) {
          entry.find('p').css('text-decoration', 'line-through');
        }
      });
    }
  });

  // Handle submit task
  var roomId = 'room' + session.user.id;
  var socket = io.connect(baseURL, { query: `roomId=${roomId}` });
  var messageList = $('#messageList');
  var messageInput = $('#messageInput');
  var messageBtn = $('#messageBtn');
  var messageAppender = function (data) {
    var entry = $(`li#task-${data.id}`);
    if (!entry.length) {
      entry = $(`<li id="task-${data.id}">`);
    }
    var raw = {
      time: data.dueAt,
      place: data.location,
      desc: data.description
    };
    entry.append('<p>' + data.raw + '</p>');
    entry.append('<pre>' + JSON.stringify(raw, 0, 2) + '</pre>');
    messageList.append(entry);
  };
  var sendMessage = function () {
    var text = messageInput.val();
    messageInput.val('');
    $.ajax({
      url: baseURL + '/task/add/' + roomId,
      type: 'POST',
      headers: {
        Authorization: 'Bearer ' + session.token
      },
      data: text,
      contentType: 'text/plain',
      success: function (res, is) {
        console.log('ajax post', res);
        socket.emit('add-message', res);
      }
    });
  };
  $('form').submit(function () {
    sendMessage();
    return false;
  });
  messageBtn.click(function () {
    sendMessage();
  });
  socket.on('add-message', function (data) {
    console.log('io add-message', data);
    messageAppender(data);
  });
  socket.on('done-message', function (data) {
    console.log('io done-message', data);
    $(`li#task-${data.id} p`).css('text-decoration', 'line-through');
  });
});
