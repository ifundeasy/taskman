$(function () {
  var baseURL = 'http://localhost:3000';

  // Handle session
  var session = localStorage.getItem('taskman');
  if (session) {
    try {
      session = JSON.parse(session);
    } catch (e) {
      console.error('Invalid localstorage value!');
      window.location = '/login';
    }

    if (session.token) {
      $.ajax({
        url: baseURL + '/token',
        type: 'GET',
        sync: true,
        headers: {
          Authorization: 'Bearer ' + session.token
        },
        statusCode: {
          401: function (res) {
            console.log(res.responseJSON.message);
            localStorage.removeItem('taskman');
          }
        },
        success: function (res, is) {
          if (res.message) window.location = '/';
        }
      });
    }
  }

  var nameInput = $('#nameInput');
  var usernameInput = $('#usernameInput');
  var passwordInput = $('#passwordInput');
  var submitBtn = $('#submitBtn');
  var registration = function () {
    var name = nameInput.val();
    var username = usernameInput.val();
    var password = passwordInput.val();

    $.ajax({
      url: baseURL + '/registration',
      type: 'POST',
      data: { name, username, password },
      statusCode: {
        500: function (res) {
          alert(res.responseJSON.message);
        }
      },
      success: function (res, is) {
        if (res.data.username === username) {
          alert('Registration success, please login');
          window.location = '/login';
        }
      }
    });
  };
  $('form').submit(function () {
    registration();
    return false;
  });
  submitBtn.click(function () {
    registration();
  });
});
