# Taskman

> Task manager with push notification.

## Installation

Create database with `taskman` name

```bash
mysqladmin -u root -p create taskman
```

Clone this repository

```bash
git clone https://gitlab.com/ifundeasy/taskman.git taskman
cd taskman
```

Copy google app credential file `google.app.credential.json` to project dir

```bash
cp /PATH_TO_YOUR/google.app.credential.json .
```

Create `.env` file (note: for your development or testing).

```bash
cp .env.example .env
vi .env
```

Edit .env file as you need.

Note: `FORCE_REMINDS` is variable to purpose app workflow demos. This is a websocket delay to marks task as done. Default is 5s delay.

Install node module

```bash
npm i
```

Sync model to database

```bash
npm run syncdb
```

## Run

You need to run in `two` terminal session for run this project

Running a web server

```bash
npm run dev
```

Running a web client

```bash
npm run client
```

Visit http://localhost:8080 in web browser, Google Chrome is recommended.

## Demo

Don't forget to do account registration http://localhost:8080/registration.html, try to open more than one tab browser.

If you have alot of time for testing, Just create more than `one` user, and try to login in incognito for seen the taskman effect with `two` different user.

Happy taskman!
